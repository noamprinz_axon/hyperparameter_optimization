import wandb

hyperparameters_defaults = dict(batch_size=2)


config_dictionary = dict(
    yaml='/home/noam/PycharmProjects/hyperopt_example/config-defaults.yaml',
    params=hyperparameters_defaults,
    )

sweep_config = {
    'method': 'random',
    'parameters': {
        'batch_size': {
            'values': [1, 2, 3, 4, 5, 6]
        }
    }
}

WANDB_PROJECT = "hyperopt_example"

def train():
    wandb.init(project=WANDB_PROJECT, config=hyperparameters_defaults)  # add sync_tensorboard=True
    wandb_config = wandb.config
    batch_size = wandb_config.batch_size
    print(batch_size)
    return


if __name__ == '__main__':

    sweep_id = wandb.sweep(sweep_config, project=WANDB_PROJECT)
    wandb.agent(sweep_id, train)
